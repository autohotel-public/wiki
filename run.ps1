New-Item -ItemType Directory -Force output
Copy-Item *.css .\output\
Copy-Item uploads .\output\  -Recurse -Force
Get-Item *.md | ForEach-Object {pandoc $_ -o $($_.DirectoryName+"\output\"+$_.BaseName+".docx");  pandoc -s  $_  -c .\pandoc.css  -o  $($_.DirectoryName+"\output\"+$_.BaseName+".html");  }
